package com.confusedpenguins.scouter;

import java.util.ArrayList;
import java.util.List;

import com.google.android.glass.app.Card;
import com.google.android.glass.widget.CardScrollAdapter;
import com.google.android.glass.widget.CardScrollView;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.provider.MediaStore;

public class ScouterHome extends Activity {

	private static final int TAKE_PICTURE_REQUEST = 1;
	
	private List<Card> mCards;
	private CardScrollView mCardScrollView;
	private Intent captureImageIntent;
	private Intent viewPowerLevelIntent;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (savedInstanceState == null) {
			
		}
		
		this.createCards();
		mCardScrollView = new CardScrollView(this);
		ScouterCardScrollAdapter adapter = new ScouterCardScrollAdapter();
		mCardScrollView.setAdapter(adapter);
		mCardScrollView.setOnItemClickListener(adapter);
		mCardScrollView.activate();
		
		captureImageIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		this.setContentView(mCardScrollView);
	}
	
	private void createCards() {
		this.mCards = new ArrayList<Card>();
		Card card = new Card(this);
		card.setText(this.getString(R.string.scan_power));
		card.setFootnote(this.getString(R.string.scan_desc));
		mCards.add(card);
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == TAKE_PICTURE_REQUEST && resultCode == RESULT_OK) {
	        //Bundle extras = data.getExtras();
	        //Bitmap imageBitmap = (Bitmap) extras.get("data");        
	        this.viewPowerLevelIntent = new Intent(this,ViewPowerlevelActivity.class);    	
	    	startActivity(viewPowerLevelIntent);
	    }
	}
	
	
	private class ScouterCardScrollAdapter extends CardScrollAdapter implements OnItemClickListener  {

		@Override
		public int getPosition(Object item) {
			return mCards.indexOf(item);
		}

		@Override
		public int getCount() {
			return mCards.size();
		}

		@Override
		public Object getItem(int position) {
			return mCards.get(position);
		}

		/**
		 * Returns the amount of view types.
		 */
		@Override
		public int getViewTypeCount() {
			return Card.getViewTypeCount();
		}

		/**
		 * Returns the view type of this card so the system can figure out if it
		 * can be recycled.
		 */
		@Override
		public int getItemViewType(int position) {
			return mCards.get(position).getItemViewType();
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			return mCards.get(position).getView(convertView, parent);
		}

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			
		    startActivityForResult(captureImageIntent, TAKE_PICTURE_REQUEST);
		}		
	}// end scroll adapter
}
