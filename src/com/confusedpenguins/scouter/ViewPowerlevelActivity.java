package com.confusedpenguins.scouter;

import java.util.Random;

import android.app.Activity;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.widget.TextView;

public class ViewPowerlevelActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_view_powerlevel);

		TextView powerLevelBase = (TextView) findViewById(R.id.powerLevelBase);
		TextView maxPowerLevel = (TextView) findViewById(R.id.maxPowerLevel);
		TextView dangerRating = (TextView) findViewById(R.id.dangerRating);

		int maxLevel = Integer.parseInt(this.getString(R.integer.power_level_max));
		int minLevel = Integer.parseInt(this.getString(R.integer.power_level_min));		
		int powerLevel = this.getRandomPowerLevel(maxLevel, minLevel);
		
		powerLevelBase.setText("Current Power Level: " + powerLevel);
		maxPowerLevel.setText("Maximum Power Level: " + maxLevel);

		if (powerLevel < 9000) {
			dangerRating.setText("Danger Rating: " + this.getString(R.string.power_rating_bad));
		} else {
			dangerRating.setText("Danger Rating: It's OVER 9000!");			
			MediaPlayer mp = MediaPlayer.create(this, R.raw.over_ninethousand);
			mp.start();
		}
	}

	private int getRandomPowerLevel(int max, int min) {
		Random rand = new Random();
		int randomNum = rand.nextInt((max - min) + 1) + min;
		return randomNum;
	}
}
